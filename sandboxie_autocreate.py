import os
import configparser
import sandboxie
import pyodbc
import time
import glob
import shutil


SANDBOX_SETTINGS_PATH = r'C:\Windows\Sandboxie.ini'
VMCONFIG_FILE_PATH = r'C:\\abc\\abcmobi.ini'
SANDBOXIES = "C:\\Sandbox\\alks\\"


def getConfig(section, key, file):
    config = configparser.ConfigParser()
    config.read(file, encoding="utf-8")
    return config.get(section, key)
    

Driver = getConfig("database", "Driver", "config.ini")
SERVER = getConfig("database", "SERVER", "config.ini")
DATABASE = getConfig("database", "DATABASE", "config.ini")
UID = getConfig("database", "UID", "config.ini")
PWD = getConfig("database", "PWD", "config.ini")
chromepath = getConfig("chrome", "chromedriverpath", "config.ini")
VMID = getConfig("VMInfo", "VMID", VMCONFIG_FILE_PATH)


class DbManager:
    def __init__(self):
        self.driver = Driver
        self.server = SERVER
        self.database = DATABASE
        self.uid = UID
        self.pwd = PWD
        self.conn = None
        self.cur = None

    # 连接数据库
    # connect to database
    def connectDatabase(self):
        co = "DRIVER={};SERVER={};DATABASE={};UID={};PWD={}".format('{' + self.driver + '}', self.server,
                                                                    self.database, self.uid, self.pwd)
        try:
            self.conn = pyodbc.connect(co)
            self.cur = self.conn.cursor()
            return True
        except:
            print("Error:Can't connect to database")
            return False

    # 关闭数据库
    # close db
    def close(self):
        # 如果数据打开，则关闭；否则没有操作
        # close connection to db
        if self.conn and self.cur:
            self.cur.close()
            self.conn.close()
        return True

    def execute(self, sql, params=None, commit=False, ):
        # 连接数据库
        # connect to db
        res = self.connectDatabase()
        if not res:
            return False
        try:
            if self.conn and self.cur:
                # 正常逻辑，执行sql，提交操作
                # if all good execute sql query
                rowcount = self.cur.execute(sql)
                self.conn.commit()
                
        except:
            print("execute failed: " + sql)
            print("params: " + str(params))
            self.close()
            return False
        return rowcount

    def fetchone(self, sql, params=None):
        res = self.execute(sql, params)
        if not res:
            print("查询失败")
            return False
        result = self.cur.fetchone()
        self.close()
        return result

    def fetchall(self, sql, params=None):
        res = self.execute(sql)
        if not res:
            print("execute failed")
            return False
        results = self.cur.fetchall()
        # print("жџҐиЇўж€ђеЉџ" + str(results))
        self.close()
        return results


def get_vmid_from_config():
    with open(VMCONFIG_FILE_PATH, 'r+', encoding='utf-8') as configfile:
        config = configparser.RawConfigParser(strict=False)
        config.read_string(configfile.read())
        return config["VMInfo"]["VMID"]


def update_sandbox_status(db_manager, sandbox_name):
    db_manager.execute("UPDATE Buyer SET TaskSandboxie = 'OK' WHERE BuyerID ='{}'".format(sandbox_name))

def get_sandboxie_range_value(local_vmid_value):
    db = DbManager()
    db.connectDatabase()
    record = db.fetchall("SELECT BuyerID FROM Buyer WHERE VMID ='{}'".format(local_vmid_value))
    return record


def create_sandbox(sandbox_name):
    db = DbManager()
    sbie = sandboxie.Sandboxie()
    for i in range(0, len(sandbox_name) + 1):
        try:
            print(sandbox_name[i][0])
            sbie.create_sandbox(box=str(sandbox_name[i][0]), options={'Enabled': 'yes'})
            update_sandbox_status(db, sandbox_name[i][0])
            time.sleep(0.3)
            if isinstance(sandbox_name[i][0], str):
                print(True)
        except:
            break


def reload_config():
    sbie = sandboxie.Sandboxie()
    print('Do you want to delete all sandboxes? Type N if dont need')
    user_answer = str(input())
    if user_answer == 'Y':
        # for data in SANDBOXIES:
        #     os.chmod(data, 0o777)
        #     os.remove(data)
        shutil.rmtree(SANDBOXIES, ignore_errors=True)
        with open(SANDBOX_SETTINGS_PATH, 'w', encoding='utf-16-le') as configfile:
            config = configparser.RawConfigParser(strict=False)
            config['GlobalSettings'] = {}
            config['DefaultBox'] = {
                'ConfigLevel': '7',
                'AutoRecover': 'y',
                'BlockNetworkFiles': 'y',
                'Template': 'qWave',
                'Template': 'WindowsFontCache',
                'Template': 'BlockPorts',
                'Template': 'LingerPrograms',
                'Template': 'Chrome_Phishing_DirectAccess',
                'Template': 'Firefox_Phishing_DirectAccess',
                'Template': 'AutoRecoverIgnore',
                'RecoverFolder': r'%{374DE290-123F-4565-9164-39C4925E467B}%',
                'RecoverFolder': r'%Personal%',
                'RecoverFolder': r'%Favorites%',
                'RecoverFolder': r'%Desktop%',
                'BorderColor': r'#00FFFF,ttl',
                'Enabled': 'y'
            }
            config['UserSettings_059A0166'] = {
                'SbieCtrl_BoxExpandedView': 'DefaultBox',
                'SbieCtrl_NextUpdateCheck': '1558189937',
                'SbieCtrl_UpdateCheckNotify': 'n',
                'SbieCtrl_ReloadConfNotify': 'n',
                'SbieCtrl_HideWindowNotify': 'n'
            }
            config.write(configfile)



reload_config()
start_sandbox_value = get_sandboxie_range_value(VMID)
create_sandbox(start_sandbox_value)
